defmodule Maplog.MixProject do
  use Mix.Project

  def project do
    [
      app: :maplog,
      version: "0.2.0",
      description: "store for keys and their values for parsed map objects",
      name: "Maplog",
      source_url: "https://gitlab.com/matteo.redaelli/maplog",
      package: package(),
      elixir: "~> 1.7",
      escript: [main_module: Maplog.CLI],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Maplog, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:jason, "~> 1.1"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "maplog",
      # These are the default files included in the package
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*
                ),
      licenses: ["GPL v3+"],
      links: %{"GitHub" => "https://gitlab.com/matteo.redaelli/maplog"}
    ]
  end
end
