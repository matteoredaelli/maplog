defmodule Maplog.Parser do
  use GenServer

  ## Client API

  @doc """
  Starts the Parser.
  """

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    {:ok, 0}
  end

  def parse_map(map), do: GenServer.cast(__MODULE__, {:parse_map, map})

  def handle_cast({:parse_map, map}, state) do
    Enum.each(
      map,
      fn {k, v} ->
        Maplog.Registry.add_value(k, v)
      end
    )

    {:noreply, state}
  end
end
