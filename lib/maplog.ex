defmodule Maplog do
  @moduledoc """
  An application for tracking keys and related values for the submitted maps
  To be used, for instance, ina Master Data management software
  """

  use Application

  def start(_type, _args) do
    Maplog.Supervisor.start_link(name: Maplog.Supervisor)
  end

  ## ,  as: :dump
  defdelegate dump, to: Maplog.Registry
  defdelegate get_keys, to: Maplog.Registry
  defdelegate get_key_values(key), to: Maplog.Registry
  defdelegate load, to: Maplog.Registry
  defdelegate save, to: Maplog.Registry
  defdelegate parse_map(map), to: Maplog.Parser
end
