# Maplog

**Parse maps and store parsed keys and values. 


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `maplog` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:maplog, "~> 18.1.1.6"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/maplog](https://hexdocs.pm/maplog).
